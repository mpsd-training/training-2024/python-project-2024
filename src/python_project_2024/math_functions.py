# from python_project_2024 import __version__
__author__ = "Heiko Appel"
__copyright__ = "Heiko Appel"
__license__ = "GPL-3.0-only"


def myfactorial(x):
    print("Computing factorial")
    if isinstance(x, int):
        if x == 1:
            return 1
        elif x > 1:
            return myfactorial(x - 1) * x
    else:
        raise ValueError("The argument of myfactorial should be an integer")
