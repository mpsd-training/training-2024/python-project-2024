.. These are examples of badges you might want to add to your README:
   please update the URLs accordingly

    .. image:: https://api.cirrus-ci.com/github/<USER>/python-project-2024.svg?branch=main
        :alt: Built Status
        :target: https://cirrus-ci.com/github/<USER>/python-project-2024
    .. image:: https://readthedocs.org/projects/python-project-2024/badge/?version=latest
        :alt: ReadTheDocs
        :target: https://python-project-2024.readthedocs.io/en/stable/
    .. image:: https://img.shields.io/coveralls/github/<USER>/python-project-2024/main.svg
        :alt: Coveralls
        :target: https://coveralls.io/r/<USER>/python-project-2024
    .. image:: https://img.shields.io/pypi/v/python-project-2024.svg
        :alt: PyPI-Server
        :target: https://pypi.org/project/python-project-2024/
    .. image:: https://img.shields.io/conda/vn/conda-forge/python-project-2024.svg
        :alt: Conda-Forge
        :target: https://anaconda.org/conda-forge/python-project-2024
    .. image:: https://pepy.tech/badge/python-project-2024/month
        :alt: Monthly Downloads
        :target: https://pepy.tech/project/python-project-2024
    .. image:: https://img.shields.io/twitter/url/http/shields.io.svg?style=social&label=Twitter
        :alt: Twitter
        :target: https://twitter.com/python-project-2024

.. image:: https://img.shields.io/badge/-PyScaffold-005CA0?logo=pyscaffold
    :alt: Project generated with PyScaffold
    :target: https://pyscaffold.org/

|

===================
python-project-2024
===================


    Python project 2024


A longer description of your project goes here...


.. _pyscaffold-notes:

Making Changes & Contributing
=============================

This project uses `pre-commit`_, please make sure to install it before making any
changes::

    pip install pre-commit
    cd python-project-2024
    pre-commit install

It is a good idea to update the hooks to the latest version::

    pre-commit autoupdate

Don't forget to tell your contributors to also install and use pre-commit.

.. _pre-commit: https://pre-commit.com/

Note
====

This project has been set up using PyScaffold 4.5. For details and usage
information on PyScaffold see https://pyscaffold.org/.
