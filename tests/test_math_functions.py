import pytest

from python_project_2024.math_functions import myfactorial

__author__ = "Heiko Appel"
__copyright__ = "Heiko Appel"
__license__ = "GPL-3.0-only"


def test_myfactorial():
    """API Tests"""
    assert myfactorial(1) == 1
    assert myfactorial(2) == 2
    assert myfactorial(4) == 24
    with pytest.raises(ValueError):
        myfactorial(1.2345)


def test_main(capsys):
    test_myfactorial()
